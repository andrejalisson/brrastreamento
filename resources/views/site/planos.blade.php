@extends('templates.site')

@section('css')
@endsection

@section('corpo')
<section class="pricing-style-one pricing-page">
    <div class="container">
        <div class="title-block text-center ">
            <span class="tag-line">Planos que cabem no seu bolso</span><!-- /.tag-line -->
            <h2>Escolha já o seu.</h2>
        </div><!-- /.title-block -->
        <div class="row">
            <div class="col-lg-3">
                <div class="single-pricing-style-one text-center  wow fadeInUp" data-wow-duration="600ms" data-wow-delay="0ms">
                    <div class="top-block">
                        <p>Rastreamento Moto</p>
                        <h3>Apenas</h3>
                        <span class="price">R$50,00</span>
                    </div><!-- /.top-block -->
                    <div class="bottom-block">
                        <ul class="feature-list">
                            <li><i class="fa fa-check"></i>Rastreamento em tempo real</li>
                            <li><i class="fa fa-check"></i>Cobertura em todo Brasil</li>
                            <li><i class="fa fa-check"></i>Aplicativo para Android ou iOS</li>
                            <li><i class="fa fa-check"></i>Alertas: ignição ligada e desligada, excesso de velocidade, corte de alimentação, entrada ou saída de cerca virtual</li>
                            <li><i class="fa fa-check"></i>Atendimento 24h</li>
                        </ul><!-- /.feature-list -->
                        <div class="button-block">
                            <a target="blank" href="https://wa.me/558588347252" class="thm-btn">Solicitar agora</a>
                        </div><!-- /.button-block -->
                    </div><!-- /.bottom-block -->
                </div><!-- /.single-pricing-style-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-pricing-style-one text-center popular-pricing wow fadeInUp" data-wow-duration="1300ms" data-wow-delay="500ms" >
                    <div class="top-block">
                        <p style="color: white">Rastreamento Moto + Assistência 24h</p>
                        <h3>Apenas</h3>
                        <span style="color: white" class="price">R$60,00</span>
                    </div><!-- /.top-block -->
                    <div class="bottom-block">
                        <ul class="feature-list">
                            <li><i class="fa fa-check"></i>Rastreamento em tempo real</li>
                            <li><i class="fa fa-check"></i>Cobertura em todo Brasil</li>
                            <li><i class="fa fa-check"></i>Aplicativo para Android ou iOS</li>
                            <li><i class="fa fa-check"></i>Alertas: ignição ligada e desligada, excesso de velocidade, corte de alimentação, entrada ou saída de cerca virtual</li>
                            <li><i class="fa fa-check"></i>Atendimento 24h</li>
                            <li><i class="fa fa-check"></i>Guincho 200km</li>
                            <li><i class="fa fa-check"></i>Aux. Mecânico</li>
                            <li><i class="fa fa-check"></i>Chaveiro</li>
                            <li><i class="fa fa-check"></i>Troca de Pneus</li>
                            <li><i class="fa fa-check"></i>Pane Seca.</li>
                            <li><i class="fa fa-check"></i>Sorteios mensais R$5.000,00 por sorteio da loteria federal</li>
                        </ul><!-- /.feature-list -->
                        <div class="button-block">
                            <a target="blank" href="https://wa.me/558588347252" class="thm-btn">Solicitar agora</a>
                        </div><!-- /.button-block -->
                    </div><!-- /.bottom-block -->
                </div><!-- /.single-pricing-style-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-pricing-style-one text-center  wow fadeInUp" data-wow-duration="1300ms" data-wow-delay="1000ms">
                    <div class="top-block">
                        <p>Rastreamento Carro</p>
                        <h3>Apenas</h3>
                        <span class="price">R$60,00</span>
                    </div><!-- /.top-block -->
                    <div class="bottom-block">
                        <ul class="feature-list">
                            <li><i class="fa fa-check"></i>Rastreamento em tempo real</li>
                            <li><i class="fa fa-check"></i>Cobertura em todo Brasil</li>
                            <li><i class="fa fa-check"></i>Aplicativo para Android ou iOS</li>
                            <li><i class="fa fa-check"></i>Alertas: ignição ligada e desligada, excesso de velocidade, corte de alimentação, entrada ou saída de cerca virtual</li>
                            <li><i class="fa fa-check"></i>Atendimento 24h</li>
                        </ul><!-- /.feature-list -->
                        <div class="button-block">
                            <a target="blank" href="https://wa.me/558588347252" class="thm-btn">Solicitar agora</a>
                        </div><!-- /.button-block -->
                    </div><!-- /.bottom-block -->
                </div><!-- /.single-pricing-style-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-pricing-style-one text-center popular-pricing wow fadeInUp" data-wow-duration="1300ms" data-wow-delay="500ms" >
                    <div class="top-block">
                        <p style="color: white">Rastreamento Carro + Assistência 24h</p>
                        <h3>Apenas</h3>
                        <span style="color: white" class="price">R$70,00</span>
                    </div><!-- /.top-block -->
                    <div class="bottom-block">
                        <ul class="feature-list">
                            <li><i class="fa fa-check"></i>Rastreamento em tempo real</li>
                            <li><i class="fa fa-check"></i>Cobertura em todo Brasil</li>
                            <li><i class="fa fa-check"></i>Aplicativo para Android ou iOS</li>
                            <li><i class="fa fa-check"></i>Alertas: ignição ligada e desligada, excesso de velocidade, corte de alimentação, entrada ou saída de cerca virtual</li>
                            <li><i class="fa fa-check"></i>Atendimento 24h</li>
                            <li><i class="fa fa-check"></i>Guincho 200km</li>
                            <li><i class="fa fa-check"></i>Aux. Mecânico</li>
                            <li><i class="fa fa-check"></i>Chaveiro</li>
                            <li><i class="fa fa-check"></i>Troca de Pneus</li>
                            <li><i class="fa fa-check"></i>Pane Seca.</li>
                            <li><i class="fa fa-check"></i>Sorteios mensais R$5.000,00 por sorteio da loteria federal</li>
                        </ul><!-- /.feature-list -->
                        <div class="button-block">
                            <a target="blank" href="https://wa.me/558588347252" class="thm-btn">Solicitar agora</a>
                        </div><!-- /.button-block -->
                    </div><!-- /.bottom-block -->
                </div><!-- /.single-pricing-style-one -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.pricing-style-one -->

@endsection

@section('js')
@endsection

@section('script')
@endsection
