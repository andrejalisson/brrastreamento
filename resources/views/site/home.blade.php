@extends('templates.site')

@section('css')
@endsection

@section('corpo')
<div class="main-banner-wrapper">
    <section class="banner-style-one owl-theme owl-carousel">
        <div class="slide slide-one" style="background-image: url(/site/images/slider/slider-1-1.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3 class="banner-title"><strong>Rastreamento </strong>Veicular</h3>
                        <p><strong>Reduza custos</strong>, tenha maior visibilidade da sua frota e garanta o <br><strong>controle da sua operação</strong> com um sistema de rastreamento veicular. </p>
                        <div class="btn-block">
                            <a target="blank" href="https://wa.me/558588347252" class="banner-btn">Solicite agora</a>
                        </div><!-- /.btn-block -->
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.slide -->
        <div class="slide slide-two" style="background-image: url(/site/images/slider/slider-1-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h3 class="banner-title"><strong>Assistência<br> 24h</strong></h3>
                        <p>Produto desenvolvido para dar agilidade no atendimento nos casos de imprevistos com o veículo, seja qual for à causa (Pane, Acidente, quebra chaves, falta de combustível, troca de pneu), Veículos novos e usados, multimarcas, nacional e importados, visando solucionar o problema o mais rápido possível e levando tranquilidade para o nosso assistido, por meio da nossa rede especializada, presente em todo o Brasil.</p>
                        <div class="btn-block">
                            <a target="blank" href="https://wa.me/558588347252" class="banner-btn">Solicite agora</a>
                        </div><!-- /.btn-block -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.slide -->
    </section><!-- /.banner-style-one -->
</div><!-- /.main-banner-wrapper -->
<section class="offer-style-one">
    <div class="container">
        <div class="title-block">
            <span class="tag-line">Rastreamento Veicular</span><!-- /.tag-line -->
            <h2>24 Horas</h2>
        </div><!-- /.title-block -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-offer-style-one wow fadeInUp" data-wow-duration="1300ms" data-wow-delay="0ms">
                    <div class="icon-block">
                        <i class="cameron-icon-placeholder"></i>
                    </div><!-- /.icon-block -->
                    <h3 style="color: black">Rastreamento <br>em tempo real</h3>
                    <p>Adquira o Rastreamento <strong>A.S Rastreamento</strong> para seu veículo e tenha tranquilidade 24h/dia, 7 dias/semana. Proteja seu carro com tecnologia de ponta!</p>
                </div><!-- /.single-offer-style-one -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6">
                <div class="single-offer-style-one wow fadeInUp" data-wow-duration="1300ms" data-wow-delay="200ms">
                    <div class="icon-block">
                        <i class="cameron-icon-smartphone"></i>
                    </div><!-- /.icon-block -->
                    <h3 style="color: black">Aplicativo <br>Android e IOS</h3>
                    <p>Fácil e rápido. Rastreie seu carro ou moto em tempo real pelo App, o App avisa quando a chave for girada ou quando o carro sair da área de segurança.</p>
                </div><!-- /.single-offer-style-one -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6">
                <div class="single-offer-style-one wow fadeInUp" data-wow-duration="1300ms" data-wow-delay="400ms">
                    <div class="icon-block">
                        <i class="cameron-icon-email"></i>
                    </div><!-- /.icon-block -->
                    <h3 style="color: black">Alertas</h3>
                    <p>Central de monitoramento 24h com pronto-atendimento em caso de sinistro.</p>
                </div><!-- /.single-offer-style-one -->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-3 col-md-6">
                <div class="single-offer-style-one wow fadeInUp" data-wow-duration="1300ms" data-wow-delay="600ms">
                    <div class="icon-block">
                        <i class="cameron-icon-support"></i>
                    </div><!-- /.icon-block -->
                    <h3 style="color: black">Central <br> 24 horas</h3>
                    <p>Central de Monitoramento e Equipe de Resgate a postos.</p>
                </div><!-- /.single-offer-style-one -->
            </div><!-- /.col-lg-3 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.offer-style-one -->


<section class="featured-style-two">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-6">
                <div class="image-block clearfix">
                    <img src="/site/images/resources/featured-1-2.jpg" alt="Awesome Image" class="float-right" />
                </div><!-- /.image-block -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 d-flex block-wrapper">
                <div class="content-block my-auto">
                    <div class="title-block">
                        <span class="tag-line">Asistência veicular</span><!-- /.tag-line -->
                    </div><!-- /.title-block -->
                    <div class="signle-featured-one">
                        <div class="text-block">
                            <h3>Assistência 24h</h3>
                            <p>Guincho 200km / Aux. Mecânico / Chaveiro / Troca de Pneus / Pane Seca.</p>
                        </div><!-- /.text-block -->
                    </div><!-- /.signle-featured-one -->
                    <div class="signle-featured-one">
                        <div class="text-block">
                            <h3>Morte Acidental</h3>
                            <p>Cobertura R$3.000,00 e Assistência Funeral Individual R$3.000,00.</p>
                        </div><!-- /.text-block -->
                    </div><!-- /.signle-featured-one -->
                    <div class="signle-featured-one">
                        <div class="text-block">
                            <h3>Invalidez</h3>
                            <p>Permanente Total ou Parcial por acidente R$ 3.000,00 para o motorista</p>
                        </div><!-- /.text-block -->
                    </div><!-- /.signle-featured-one -->
                    <div class="signle-featured-one">
                        <div class="text-block">
                            <h3>Capitalização</h3>
                            <p>Concorra todos os meses R$ 5.000,00 por sorteio da loteria federal.</p>
                        </div><!-- /.text-block -->
                    </div><!-- /.signle-featured-one -->
                    <div class="signle-featured-one">
                        <div class="text-block">
                            <h3>Território Brasileiro</h3>
                            <p>Atendimento em todo o território Brasileiro com 0800 24 horas.</p>
                        </div><!-- /.text-block -->
                    </div><!-- /.signle-featured-one -->
                    <div class="signle-featured-one">
                        <div class="text-block">
                            <h3>Acionamentos</h3>
                            <p>Vigência de 12 meses / 1 acionamento por mês / 3 por serviço no Ano.</p>
                        </div><!-- /.text-block -->
                    </div><!-- /.signle-featured-one -->
                </div><!-- /.content-block -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.featured-style-two -->
<section class="testimonials-style-one home-page-two">
    <div class="container">
        <div class="title-block text-center">
            <span class="tag-line">Estão falando da gente :)</span><!-- /.tag-line -->
        </div><!-- /.title-block -->
        <div class="owl-carousel owl-theme testi-carousel-one">
            <div class="item">
                <div class="single-testimonials-one">
                    <p>O Atendimento é impecável. Profissionalismo exemplar. A A.S Rastreamento é nota mil.</p>
                    <h3>Daniel Lopes</h3>
                </div><!-- /.single-testimonials-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-testimonials-one">
                    <p>Em menos de um mês com a A.S Rastreamento já percebi o resultado. A A.S Rastreamento nos ajuda a manter o controle total de nossa frota.</p>
                    <h3>Cícero Vieira</h3>
                </div><!-- /.single-testimonials-one -->
            </div><!-- /.item -->
        </div><!-- /.owl-carousel owl-theme testi-carousel-one -->
    </div><!-- /.container -->
</section><!-- /.testimonials-style-one -->
@endsection

@section('js')
@endsection

@section('script')
@endsection
