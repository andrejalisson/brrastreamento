@extends('templates.site')

@section('css')
@endsection

@section('corpo')

<section class="about-style-one">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex">
                <div class="content-block my-auto">
                    <div class="title-block">
                        <span class="tag-line">Atendimento 0800 24h</span><!-- /.tag-line -->
                        <h2>Fique zen e deixe<br> tudo <strong>com a gente</strong></h2>
                    </div><!-- /.title-block -->
                    <p>Nós temos o melhor atendimento 0800 para sua empresa. Assistência 24/7 para seus veículos em parceria com a SulAmerica Seguros.</p>
                    <a target="blank" href="https://wa.me/558588347252" class="more-btn">Solicite agora</a>
                </div><!-- /.content-block -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="image-block">
                    <img src="/site/images/resources/story-1-1.png" alt="Awesome Image" />
                </div><!-- /.image-block -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-style-one -->
<section class="about-style-one">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="image-block">
                    <img src="/site/images/resources/story-1-2.png" alt="Awesome Image" />
                </div><!-- /.image-block -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 d-flex">
                <div class="content-block my-auto">
                    <div class="title-block">
                        <span class="tag-line">FIQUE TRANQUILO COM UM SERVIÇO DE ATENDIMENTO 0800 24/7</span><!-- /.tag-line -->
                        <h2>O melhor custo benefício do mercado.</h2>
                    </div><!-- /.title-block -->
                    <p>Nossa central de atendimento 0800 tem uma equipe de atendentes 24/7 para atender melhor lhe atender.</p>
                    <a target="blank" href="https://wa.me/558588347252" class="more-btn">Solicite agora</a>
                </div><!-- /.content-block -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-style-one -->

<section class="contact-info-style-one">
    <div class="container">
        <div class="title-block text-center">
            <span class="tag-line">ASSISTÊNCIA 24 HORAS SULAMÉRICA VIDA + AUTO</span><!-- /.tag-line -->
            <h2>Cobertura em todo território Brasileiro 24/7</h2>
        </div><!-- /.title-block -->
        <div class="row high-gutter">
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>
                        <h3>Assistência 24h</h3>
                        <p>Guincho 200km / Aux. Mecânico / Chaveiro / Troca de Pneus / Pane Seca</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>
                        <h3>Morte Acidental</h3>
                        <p>Cobertura R$3.000,00 e Assistência Funeral Individual R$3.000,00</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Capitalização</h3>
                        <p>Concorra todos os meses R$ 5.000,00 por sorteio da loteria federal</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Território Brasileiro</h3>
                        <p>Atendimento em todo o território Brasileiro com 0800 24 horas</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Acionamentos</h3>
                        <p>Vigência de 12 meses / 1 acionamento por mês / 3 por serviço no Ano</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Invalidez</h3>
                        <p>Permanente Total ou Parcial por acidente R$ 3.000,00 para o motorista</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Apólice do Segurado</h3>
                        <p>Todas as Placas fechadas e inseridas recebem uma Apólice pela SulAmerica</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Atendimento</h3>
                        <p>Todos os contatos são diretamente realizados pela SulAmerica</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.contact-info-style-one -->

@endsection

@section('js')
@endsection

@section('script')
@endsection
