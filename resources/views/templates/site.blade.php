
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}} | A.S Rastreamento</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/site/images/favicon//apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/site/images/favicon//apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/site/images/favicon//apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/site/images/favicon//apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/site/images/favicon//apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/site/images/favicon//apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/site/images/favicon//apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/site/images/favicon//apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/site/images/favicon//apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/site/images/favicon//android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/site/images/favicon//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/site/images/favicon//favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/site/images/favicon//favicon-16x16.png">
    <link rel="manifest" href="/site/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/site/css/style.css">
    <link rel="stylesheet" href="/site/css/responsive.css">
    @yield('css')
</head>

<body>
    <div class="preloader"></div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="main-header header-style-two">
            <div class="top-header">
                <div class="container">
                    <div class="left-info">
                        <p><i class="cameron-icon-email"></i><a href="mailto:contato@asrastreamento.com.br">contato@asrastreamento.com.br</a></p>
                    </div><!-- /.left-info -->
                    <div class="right-info">
                        <ul class="info-block">
                            <li><i class="cameron-icon-support"></i><a target="blank" href="https://wa.me/558588347252">(85) 9 8834-7252</a></li>
                            {{-- <li><a href="#" class="cart-btn"><i class="cameron-icon-shopping-bag"></i><span class="count-text">(0)</span></a></li> --}}
                        </ul>
                    </div><!-- /.right-info -->
                </div><!-- /.container -->
            </div><!-- /.top-header -->
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="#">
                            <img src="/site/images/resources/logo-1-1.png" class="main-logo" alt="Awesome Image" />
                            <img src="/site/images/resources/logo-1-2.png" class="stricky-logo" alt="Awesome Image" />
                        </a>
                        <button class="menu-toggler" data-target=".header-style-two .main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div><!-- /.logo-box -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                        <ul class=" navigation-box">
                            <li @if ($title == "Home")
                            class="current"
                            @endif>
                                <a href="/">Home</a>
                            </li>
                            <li @if ($title == "Sobre")
                            class="current"
                            @endif>
                                <a href="/Sobre">Sobre</a>
                            </li>
                            <li @if ($title == "Planos")
                            class="current"
                            @endif>
                                <a href="/Planos">Planos</a>
                                <ul class="sub-menu">
                                    <li><a href="/Planos">Carro</a></li>
                                    <li><a href="/Planos">Moto</a></li>
                                </ul><!-- /.sub-menu -->
                            </li>
                            <li @if ($title == "Assistência 24h")
                            class="current"
                            @endif>
                                <a href="/Assistencia24h">Assistência 24h</a>
                            </li>
                            <li @if ($title == "Entre em Contato")
                            class="current"
                            @endif>
                                <a href="/Contato">Contato</a>
                            </li>
                            <li>
                                <a target="blank" href="http://assv.asrastreamento.com.br/">Área do Cliente</a>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="right-side-box">
                        <div class="social">
                            <a target="blank" href="https://www.facebook.com/asrastreamento/"><i class="fa fa-facebook-f"></i></a>
                            <a target="blank" href="https://www.instagram.com/asrastreamento/"><i class="fa fa-instagram"></i></a>
                            {{-- <a target="blank" href="#"><i class="fa fa-youtube"></i></a> --}}
                        </div><!-- /.social -->
                    </div><!-- /.right-side-box -->
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.main-header header-style-two -->





        @yield('corpo')


        <footer class="site-footer">
            <div class="main-footer">
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-lg-3 col-md-6">
                            <div class="footer-widget about-widget">
                                <a href="/site/index.html" class="footer-logo">
                                    <img src="/site/images/resources/footer-logo-1-1.png" alt="awesome image">
                                </a>
                                <p><i class="fas fa-map-marker-alt"></i>ENDEREÇO:<br>
                                    <small>R. Teofredo Goiana, 1501 - Cidade dos Funcionários. Fortaleza/CE</small> <br>
                                    <i class="fas fa-phone-alt"></i>SAC:<br>
                                    <small>+55 (85) 9 8834-7252</small><br>
                                    <i class="fas fa-mail-bulk"></i>Email:<br>
                                    <small>contato@asrastreamento.com.br</small></p>
                                <div class="social-block">
                                    <a target="blank" href="https://www.facebook.com/asrastreamento/"><i class="fa fa-facebook-f"></i></a>
                                    <a target="blank" href="https://www.instagram.com/asrastreamento/"><i class="fa fa-instagram"></i></a>
                                    {{-- <a target="blank" href="/site/#"><i class="fa fa-youtube"></i></a> --}}
                                </div><!-- /.social-block -->
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-3 -->

                        <div class="col-lg-3 col-md-6">
                            <center>
                            <div class="footer-widget works-widget">
                                <div class="footer-widget-title">
                                    <h3>Instagram</h3>
                                </div><!-- /.footer-widget-title -->
                                <div class="gallery-wrapper clearfix">
                                    <!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/162b15947e695836a1ca18b2fd188eb0.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
                                </div><!-- /.gallery-wrapper -->
                            </div><!-- /.footer-widget -->
                        </center>
                        </div><!-- /.col-lg-3 -->
                        <div class="col-lg-2 col-md-6">
                            <div class="footer-widget newsletter-widget">
                                <center>
                                    <div class="footer-widget-title">
                                        <h3>Fique por dentro.</h3>
                                    </div><!-- /.footer-widget-title -->
                                </center>
                                <form action="/Newsletter" method="POST" class="newsletter-form">
                                    {!! csrf_field() !!}
                                    <p><small>Fique por dentro de tudo que acontece no mundo do rastreamento.</small></p>
                                    <input type="email" name="email" placeholder="Coloque seu email">
                                    <button type="submit">Inscrever-se</button>
                                </form><!-- /.newsletter-form -->
                            </div><!-- /.footer-widget newsletter-widget -->
                        </div><!-- /.col-lg-3 -->
                        <div class="col-lg-1 col-md-6">
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="footer-widget newsletter-widget">
                                <div class="footer-widget-title">
                                    <center><h3>Facebook</h3></center>
                                </div><!-- /.footer-widget-title -->
                                <div id="fb-root"></div>
                                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v7.0&appId=348801248988519&autoLogAppEvents=1"></script>
                                <div class = "fb-page" data-href = "https://www.facebook.com/asrastreamento" data-tabs = "linha do tempo" data-width = "" data-height = "" data-small-header = "false" data-adap-container-width = "true" data-hide-cover = "false" data-show-facepile = "true" > <blockquote cite = "https://www.facebook.com/asrastreamento " class = " fb-xfbml-parse-ignore " > <a href =            "https://www.facebook.com/asrastreamento" > asrastreamento </a> </blockquote> </div>
                            </div><!-- /.footer-widget newsletter-widget -->
                        </div><!-- /.col-lg-3 -->


                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.main-footer -->
            <div class="bottom-footer text-center">
                <div class="container">
                    <p><a href="/site/#">A.S Rastreamento</a> &copy; {{date('Y')}} Todos os Direitos Reservados.</p>
                </div><!-- /.container -->
            </div><!-- /.bottom-footer -->
        </footer><!-- /.site-footer -->
    </div><!-- /.page-wrapper -->
    <!-- /.scroll-to-top -->
    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    <!-- /.scroll-to-top -->
    <script src="/site/js/jquery.js"></script>
    <script src="/site/js/bootstrap.bundle.min.js"></script>
    <script src="/site/js/owl.carousel.min.js"></script>
    <script src="/site/js/waypoints.min.js"></script>
    <script src="/site/js/jquery.counterup.min.js"></script>
    <script src="/site/js/wow.js"></script>
    <script src="/site/js/theme.js"></script>
    @yield('js')
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v7.0'
        });
      };

      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/pt_BR/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
        attribution=setup_tool
        page_id="105666304488075"
        logged_in_greeting="Quer falar com um vendedor?"
        logged_out_greeting="Quer falar com um vendedor?">
    </div>
    @yield('script')
</body>

</html>
