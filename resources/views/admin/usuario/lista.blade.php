@extends('templates.admin')

@section('css')
    <link href="/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="usuarios" class="table table-striped table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th style='width: 5%;'>#</th>
                        <th style='width: 5%;'>Imagem</th>
                        <th>Nome</th>
                        <th>Usuário</th>
                        <th>Email</th>
                        <th style='width: 5%;'>Status</th>
                        <th style='width: 15%;'>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')
<script>
$(function () {
    $(document).ready(function () {
        $('#usuarios').dataTable({
            pageLength: 100,
            responsive: true,
            processing: true,
            serverSide: true,
            order: [[ 0, "desc" ]],
            oLanguage: {
            "sLengthMenu": "Mostrar _MENU_ registros por página",
            "sZeroRecords": "Nenhum registro encontrado",
            "sInfo": "Mostrando _END_ de _TOTAL_ registro(s)",
            "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros)",
            "sSearch": "Pesquisar: ",
            "oPaginate": {
                "sFirst": "Início",
                "sPrevious": "Anterior",
                "sNext": "Próximo",
                "sLast": "Último"
                }
            },
            ajax:{
                 "url": "{{ url('todosUsuarios') }}",
                 "dataType": "json",
                 "type": "POST",
                 "data":{
                        _token: "{{csrf_token()}}"
                 }
               },
            columns: [
                { "data": "id" },
                { "data": "imagem" },
                { "data": "nome" },
                { "data": "usuario" },
                { "data": "email" },
                { "data": "status" },
                { "data": "opcoes" }
            ]
        });
    });
});
</script>
@endsection
