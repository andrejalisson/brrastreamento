@extends('templates.admin')

@section('css')
<link href="/css/plugins/cropper/cropper.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <form method="get">
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="col-form-label">Nome</label>
                                    <input type="text" id="nome" value="{{$usuario->nome_user}}" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label class="col-form-label">Email</label>
                                    <input type="email" id="email" value="{{$usuario->email_user}}" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label class="col-form-label">Usuário</label>
                                    <input type="text" id="usuario" value="{{$usuario->usuario_user}}" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label class="col-form-label">Senha </label>
                                    <input type="password" id="senha" placeholder="Senha" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label class="col-form-label">Repetir Senha</label>
                                    <input type="password" id="resenha" placeholder="Repetir Senha" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="image-crop">
                                <img src="{{$usuario->imagem_user}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Pré Visualização</h4>
                            <div class="img-preview img-preview-sm"></div>
                            <div class="hr-line-dashed"></div>
                            <div>
                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                    <input type="file" accept="image/*" name="file" id="inputImage" style="display:none">
                                    Mudar Imagem
                                </label>
                            </div>
                            <h4>Other method</h4>
                            <div class="btn-group">
                                <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
                                <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
                                <button class="btn btn-white" id="rotateLeft" type="button">Rodar Esquerda</button>
                                <button class="btn btn-white" id="rotateRight" type="button">Rodar Direita</button>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group row">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary btn-sm" id="salvar" type="button">Salvar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Image cropper -->
<script src="/js/plugins/cropper/cropper.min.js"></script>
@endsection

@section('script')
<script>
    $(document).ready(function(){

        var $image = $(".image-crop > img")
        var $cropped = $($image).cropper({
            aspectRatio: 1.1,
            preview: ".img-preview",
            done: function(data) {
                // Output the result data for cropping image.
            }
        });

        var $inputImage = $("#inputImage");
        if (window.FileReader) {
            $inputImage.change(function() {
                var fileReader = new FileReader(),
                        files = this.files,
                        file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function () {
                        $inputImage.val("");
                        $image.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Por favor, escolha um arquivo de imagem.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $("#download").click(function (link) {
            link.target.href = $cropped.cropper('getCroppedCanvas', { width: 48, height: 48 }).toDataURL("/image/png").replace("image/png", "application/octet-stream");
            link.target.download = 'cropped.png';
        });


        $("#zoomIn").click(function() {
            $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function() {
            $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function() {
            $image.cropper("rotate", 90);
        });

        $("#rotateRight").click(function() {
            $image.cropper("rotate", -90);
        });

        $("#salvar").click(function() {
            if($("#senha").val() == $("#resenha").val()) {
                $.post("/EditarUsuario",{
                    imagem: $image.cropper("getCroppedCanvas").toDataURL(),
                    id: {{$usuario->id_user}},
                    nome: $("#nome").val(),
                    email: $("#email").val(),
                    usuario: $("#usuario").val(),
                    senha: $("#senha").val(),
                    _token: "{{csrf_token()}}"
                },
                function(data){
                    swal({
                        title: "Sucesso!",
                        text: "Salvo",
                        type: "success"
                    });
                    $(location).prop('href', 'http://www.asrastreamento.com.br/Usuarios')
                });     
            }else{
                swal({
                title: "Erro!",
                text: "As senhas não coincidem",
                type: "error"
            });
            }
           
        });


    });





</script>
@endsection
