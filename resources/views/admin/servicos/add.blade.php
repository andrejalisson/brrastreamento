@extends('templates.admin')

@section('css')

@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-12 b-r"><h3 class="m-t-none m-b">Cadastrar Serviços</h3>
                        <form role="form" method="POST" action="\AdicionarServicos">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label>Título</label>
                                <input type="text" placeholder="Título" name="titulo" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Descrição</label>
                                <input type="text" placeholder="Descrição" name="descricao" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Valor</label>
                                <input type="number" name="valor" placeholder="Valor" class="form-control">
                            </div>
                            <div>
                                <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit"><strong>Salvar</strong></button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')

@endsection

@section('script')

@endsection
