<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiteController extends Controller{

    public function home(){
        $title = "Home";
        return view('site.home')->with(compact( 'title'));
    }

    public function sobre(){
        $title = "Sobre";
        return view('site.sobre')->with(compact( 'title'));
    }

    public function planos(){
        $title = "Planos";
        return view('site.planos')->with(compact( 'title'));
    }

    public function assistencia(){
        $title = "Assistência 24h";
        return view('site.assistencia')->with(compact( 'title'));
    }

    public function contato(){
        $title = "Entre em Contato";
        return view('site.contato')->with(compact( 'title'));
    }

    public function mensagemPost(Request $request){
        DB::table('contato')->insert([
            'nome_con' => $request->nome,
            'email_con' => $request->email,
            'telefone_con' => $request->telefone,
            'mensagem_con' => $request->mensagem,
            'criado_con' => date("Y-m-d H:i:s"),
            ]
        );
        $request->session()->flash('sucesso', 'Sua mensagem foi enviada!');
        return redirect('/');
    }

    public function newsletterPost(Request $request){
        DB::table('newsletter')->insert([
            'email_new' => $request->email,
            'criado_new' => date("Y-m-d H:i:s"),
            ]
        );
        $request->session()->flash('sucesso', 'Parabéns, você vai ser informado das novidades.');
        return redirect('/');
    }

}
