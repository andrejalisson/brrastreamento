<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServicoController extends Controller{
    public function servico(){
        $title = "Serviços";
        return view('admin.servicos.lista')->with(compact( 'title'));
    }

    public function add(){
        $title = "Adicionar Serviços";
        return view('admin.servicos.add')->with(compact( 'title'));
    }

    public function edt($id){
        $title = "Editar Serviços";
        $servico = DB::table('Servicos')->where('id_ser', $id)->first();
        return view('admin.servicos.edt')->with(compact('title', 'servico'));
    }

    public function edtPost(Request $request, $id){
        DB::table('Servicos')
              ->where('id_ser', $id)
              ->update([
                'titulo_ser' => $request->titulo,
                'descrição_ser' => $request->descricao,
                'valor_ser' => $request->valor,
                  ]);
        
        $request->session()->flash('sucesso', 'Serviço Editado.');
        return redirect('/Servicos');

    }

    public function excluirPost(Request $request, $id){
        DB::table('Servicos')
              ->where('id_ser', $id)
              ->update([
                'status_ser' => 0,
                  ]);
        $request->session()->flash('sucesso', 'Serviço Excluído.');
        return redirect('/Servicos');
    }

    public function adicionarServicos(Request $request){
        DB::table('Servicos')->insert([
            'titulo_ser' => $request->titulo,
            'descrição_ser' => $request->descricao,
            'valor_ser' => $request->valor,
            'criado_ser' => date("Y-m-d H:i:s"),
            'status_ser' => 1,
            ]
        );
        $request->session()->flash('sucesso', 'Serviço salvo');
        return redirect('/Servicos');
    }

    public function lixo(){
        $title = "Lixeira de Serviços";
        return view('admin.servicos.lixeira')->with(compact( 'title'));
    }

    public function todosServicosLixeira(Request $request){
        $columns = array(
            0 =>'id_ser',
            1 =>'titulo_ser',
            2 =>'descrição_ser',
            3 =>'valor_ser',
        );        
        $totalData = DB::table('Servicos')
                        ->where('status_ser', 0)
                        ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $servicos = DB::table('Servicos')
                            ->where('status_ser', 0)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $servicos =  DB::table('Servicos')
                            ->where('titulo_ser','LIKE',"%{$search}%")
                            ->orwhere('descrição_ser','LIKE',"%{$search}%")
                            ->orwhere('valor_ser','LIKE',"%{$search}%")
                            ->where('status_ser', 0)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            $totalFiltered = DB::table('Servicos')
                            ->where('titulo_ser','LIKE',"%{$search}%")
                            ->orwhere('descrição_ser','LIKE',"%{$search}%")
                            ->orwhere('valor_ser','LIKE',"%{$search}%")
                            ->where('status_ser', 0)
                            ->count();
            
        }
        $data = array();
        if(!empty($servicos)){
            foreach ($servicos as $servico){
                $nestedData['id'] = "# ".$servico->id_ser;
                $nestedData['titulo'] = $servico->titulo_ser;
                $nestedData['descricao'] = $servico->descrição_ser;
                $nestedData['valor'] = "R$ ".number_format($servico->valor_ser, 2, ',', '.');
               
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }

    public function todosServicos(Request $request){
        $columns = array(
            0 =>'id_ser',
            1 =>'titulo_ser',
            2 =>'descrição_ser',
            3 =>'valor_ser',
        );        
        $totalData = DB::table('Servicos')
                        ->where('status_ser', 1)
                        ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $servicos = DB::table('Servicos')
                            ->where('status_ser', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $servicos =  DB::table('Servicos')
                            ->where('titulo_ser','LIKE',"%{$search}%")
                            ->orwhere('descrição_ser','LIKE',"%{$search}%")
                            ->orwhere('valor_ser','LIKE',"%{$search}%")
                            ->where('status_ser', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            $totalFiltered = DB::table('Servicos')
                            ->where('titulo_ser','LIKE',"%{$search}%")
                            ->orwhere('descrição_ser','LIKE',"%{$search}%")
                            ->orwhere('valor_ser','LIKE',"%{$search}%")
                            ->where('status_ser', 1)
                            ->count();
            
        }
        $data = array();
        if(!empty($servicos)){
            foreach ($servicos as $servico){
                $nestedData['id'] = "# ".$servico->id_ser;
                $nestedData['titulo'] = $servico->titulo_ser;
                $nestedData['descricao'] = $servico->descrição_ser;
                $nestedData['valor'] = "R$ ".number_format($servico->valor_ser, 2, ',', '.');
                $nestedData['opcoes'] = "   <a class=\"btn btn-warning btn-circle\" href=\"/EditarServicos/".$servico->id_ser."\" type=\"button\"><i class=\"fa fa-pencil\"></i></a>
                                            <a class=\"btn btn-danger btn-circle\" href=\"/ExcluirServicos/".$servico->id_ser."\" type=\"button\"><i class=\"fa fa-times\"></i></a>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }
}
