<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->id('id_con');
            $table->string('nome_con')->nullable();
            $table->string('email_con')->nullable();
            $table->string('telefone_con')->nullable();
            $table->string('mensagem_con')->nullable();
            $table->dateTime('criado_con')->nullable();
            $table->softDeletes('deletado_con', 0);
            $table->boolean('status_con')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contato');
    }
}
